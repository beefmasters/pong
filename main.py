import kivy
kivy.require('1.1.1')

from kivy.config import Config
Config.set('graphics', 'width', '700')
Config.set('graphics', 'height', '500')

from kivy.core.audio import SoundLoader

#install_twisted_rector must be called before importing the reactor
from kivy.support import install_twisted_reactor
install_twisted_reactor()

#A simple Client that send messages to the echo server
from twisted.internet import reactor, protocol

class ServerEchoProtocol(protocol.Protocol):
    def connectionMade(self):
        self.factory.app.server_on_connection(self.transport)

    def dataReceived(self, data):
        response = self.factory.app.handle_message(data)
        if response:
            self.transport.write(response)

class ServerEchoFactory(protocol.Factory):
    protocol = ServerEchoProtocol

    def __init__(self, app):
        self.app = app

class EchoClient(protocol.Protocol):
    def connectionMade(self):
        self.factory.app.on_connection(self.transport)

    def dataReceived(self, data):
        #self.factory.app.print_message(data) #comment this line out
        self.factory.app.handle_message(data)


class EchoFactory(protocol.ClientFactory):
    protocol = EchoClient

    def __init__(self, app):
        self.app = app

    def clientConnectionLost(self, conn, reason):
        self.app.print_message("connection lost")

    def clientConnectionFailed(self, conn, reason):
        self.app.print_message("connection failed, starting server...")
        self.app.start_server()


from kivy.app import App
from kivy.uix.widget import Widget
from kivy.properties import NumericProperty, ReferenceListProperty,\
    ObjectProperty
from kivy.vector import Vector
from kivy.clock import Clock
import time
from datetime import datetime
import json, threading

'''
TODO:
scale application by window size so that it runs at the same speed in any window
(speed would get calculated according to a ratio to number of pixels)
'''
sound = SoundLoader.load('sounds/click.mp3')
sound.volume = 0.5
winSound = SoundLoader.load('sounds/whistle.mp3')
winSound.volume = 0.6

def send_ball_position(connection, Bx, By, Vx, Vy):
    print datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
    print('Server: {"Bx":' + str(Bx) +
    ', "By":' + str(By) +
    ', "Vx":' + str(Vx) +
    ', "Vy":' + str(Vy) +
    '}\n'), '---------------------'
    connection.write(
    '{"Bx":' + str(Bx) +
    ', "By":' + str(By) +
    ', "Vx":' + str(Vx) +
    ', "Vy":' + str(Vy) +
    '}\n')

class PongPaddle(Widget):
    score = NumericProperty(0)

    def bounce_ball(self, ball, connection):
        if self.collide_widget(ball):
            # if sound:
                # print("Sound found at %s" % sound.source)
                # print("Sound is %.3f seconds" % sound.length)
            if sound.state == 'play':
                sound.stop()
            sound.play()
            vx, vy = ball.velocity
            offset = 0
            if self.height > 2:
                offset = 1 * (ball.center_y - self.center_y) / 15 #/ (self.height / 2)
            bounced = Vector(-1 * vx, vy)
            vel = bounced * 1.1
            '''
            #FIXME: this increase of velocity should be scaled by width or something
            large width games will start with a higher velocity, which will scale up faster
            because they started as a higher number. Should scale up equally no matter
            what the width is.
            Maybe add a button to scale speed by linear function.
            '''
            if abs(vel.x) > 400.0:
                vel = bounced * 1.0
            ball.velocity = vel.x, vel.y + offset
            #print vel.x

            if connection:
                send_ball_position(connection, ball.pos[0], ball.pos[1], ball.velocity_x, ball.velocity_y)


class PongBall(Widget):
    velocity_x = NumericProperty(0)
    velocity_y = NumericProperty(0)
    velocity = ReferenceListProperty(velocity_x, velocity_y)

    def move(self):
        self.pos = Vector(*self.velocity) + self.pos

    def getnextpos(self):
        return Vector(*self.velocity) + self.pos


class PongGame(Widget):
    connection = None
    clientmode = 0

    ball = ObjectProperty(None)
    player1 = ObjectProperty(None)
    player2 = ObjectProperty(None)

    def connect_to_server(self):
        reactor.connectTCP('localhost', 8000, EchoFactory(self))

    def start_server(self):
        reactor.listenTCP(8000, ServerEchoFactory(self))

    def on_connection(self, connection):
        self.print_message("connected succesfully!")
        self.connection = connection
        self.clientmode = 1

    def server_on_connection(self, connection):
        self.print_message("connected succesfully!")
        self.connection = connection

    def send_paddle_pos_message(self, *args):
        if self.clientmode == 1:
            msg = '{"P":' + str(self.player1.center_y) + '}\n'
        else:
            msg = '{"P":' + str(self.player2.center_y) + '}\n'
        if msg and self.connection:
            self.connection.write(msg)

    def send_score_message(self, *args):
        # So, only the server will call this function, this will trigger
        # the client to reset it's own paddle position to center
        msg = '{"S":' + "0" + '}\n'
        if msg and self.connection:
            self.connection.write(msg)

    def handle_message(self, msg):
        print datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
        if self.clientmode == 0:
            print "Server:", msg, '---------------------'
        else:
            print "Client:", msg, '---------------------'

        # starting out I'll just look at the last value. Will need to expand
        # so that this processes the last of each type.
        # print "In handle message:", threading.current_thread()
        paddlepos = None
        ballpos = None
        for line in msg.split("\n"):
            if len(line) > 0:
                try:
                    d = json.loads(line)
                    if 'P' in d:
                        paddlepos = d
                    elif 'Bx' in d and 'By' in d:
                        ballpos = d
                    elif 'S' in d:
                        self.player1.center_y = self.center_y
                        self.player2.center_y = self.center_y
                    else:
                        print "I DONT KNOW HOW TO HANDLE THAT STRING"
                except:
                    print 'Failed to json loads this message!'

        if paddlepos:
            if self.clientmode == 1:
                self.player2.center_y = paddlepos['P']
            else:
                self.player1.center_y = paddlepos['P']

        if self.clientmode == 1 and ballpos:
            self.ball.center[0] = ballpos['Bx']
            self.ball.center[1] = ballpos['By']
            self.ball.velocity_x = ballpos['Vx']
            self.ball.velocity_y = ballpos['Vy']

    def print_message(self, msg):
        #could probably get rid of this function
        #self.label.text += msg + "\n"
        print 'print_message:', msg

    def setPos(self, instance, value):
        self.player1.center_y = self.center_y
        self.player2.center_y = self.center_y
        self.ball.center = self.center
        self.ball.speed = self.width / 150#213
        self.ball.velocity = (self.ball.speed, 0)
    #def init(self):
    #    self.player1.size =  [int(self.height / 24), int(self.height / 4)]
    #    self.player2.size =  [int(self.height / 24), int(self.height / 4)]
    #    self.ball.size =  [int(self.height / 15), int(self.height / 15)]

    def serve_ball(self, player=3, connection=None):
        mode = 0
        reset_paddle_pos = 1
        winSound.play()

        if reset_paddle_pos == 1:
            self.player1.center_y = self.center_y
            self.player2.center_y = self.center_y
            self.send_score_message()

        if player == 3:
            #if the game has just started
            '''
            ok so this next line will get executed and then immediately following,
            setPos will get called and it will changed the speed of the ball.
            '''
            speed = self.ball.speed
        else:
            speed = self.ball.speed
            '''
            might be kind of funny to make the winner get smaller every point
            could also make the ball grow proportionately to how much the loser
            is losing as it goes towards the loser. (shrinks as it moves back
            towards the winner)
            Add buttons in the middle for different mode enabling.
            Make the button on the bottom so the ball passes over it (toggles)
            '''
        self.ball.center = self.center
        if player == 0:
            #player2 scored.
            self.ball.velocity = (speed, 0)
            if mode == 1:
                # player 2 scored so his paddle becomes smaller
                # player 1 goes back to original size since he got scored on.
                newheight = int(self.player2.size[1] * .75)
                if newheight > 0:
                    self.player2.size = [self.player2.size[0], newheight]
                self.player1.size = self.player1.defsize
                if reset_paddle_pos == 1:
                    self.player1.center_y = self.center_y
                    self.player2.center_y = self.center_y
        else:
            self.ball.velocity = (speed * -1, 0)
            if mode == 1:
                newheight = int(self.player1.size[1] * .75)
                if newheight > 0:
                    self.player1.size = [self.player1.size[0], newheight]
                self.player2.size = self.player2.defsize
                if reset_paddle_pos == 1:
                    self.player1.center_y = self.center_y
                    self.player2.center_y = self.center_y
        self.ball.moved = False

        if connection:
            send_ball_position(connection, self.center_x, self.center_y, self.ball.velocity_x, self.ball.velocity_y)

    def update(self, dt):
        '''
        It looks like there is only one thread, so you should be able
        to change values wherever you want.
        '''


            # Add velocity values to this and handle them. Then move this call into bounce_ball()

        '''
        TODO: check a variable here, set it to 60 or 120 when score,
        if it is > 0, do nothing, make the ball velocity 0 and just write
        player 1 scores or something for a second or two (leave ball in win pos)
        '''
            #self.ball.move()
        if self.clientmode == 0:
            v = self.ball.getnextpos()
            if v.x > self.width or v.x < self.x:
                if self.ball.moved == True:
                    if v.x > self.width:
                        self.player1.score += 1
                        self.serve_ball(1, self.connection)
                    else:
                        self.player2.score += 1
                        self.serve_ball(0, self.connection)
                else:
                    if v.x > self.width:
                        self.ball.pos = [self.width-2, self.ball.pos[1]] #FIXME: should this be v.y ? what if v.y is beyond the top or bottom?
                        self.ball.moved = True
                        #print self.ball.pos
                    else:
                        self.ball.pos = [self.x+2, self.ball.pos[1]]
                        self.ball.moved = True
                        #print self.ball.pos
            else:
                self.ball.move()
                self.ball.moved = False
        else:
            self.ball.move()

        if self.clientmode == 0:
            #bounce ball off paddles
            self.player1.bounce_ball(self.ball, self.connection)
            self.player2.bounce_ball(self.ball, self.connection)

        #bounce ball off bottom or top
        if (self.ball.y < self.y) or (self.ball.top > self.top):
            self.ball.velocity_y *= -1

        if self.clientmode == 0:
            #went off a side to score point?
            if self.ball.x < self.x:
                #print "SCORED:", self.ball.x
                self.player2.score += 1
                self.serve_ball(0, self.connection)
            if self.ball.x > self.width:
                self.player1.score += 1
                self.serve_ball(1, self.connection)


    def on_touch_move(self, touch):
        # touch.sy and touch.sx are relative positions (between 0 and 1)
        # you can probably use those to play between different resolutions
        #print(touch.x, touch.y)
        if touch.x < self.width / 3:
            self.player1.center_y = touch.y
        if touch.x > self.width - self.width / 3:
            self.player2.center_y = touch.y


class PongApp(App):


    def build(self):
        game = PongGame()
        game.bind(size=game.setPos, pos=game.setPos)
        game.connect_to_server()
        game.serve_ball()
        game.bind(on_touch_move=game.send_paddle_pos_message)
        Clock.schedule_interval(game.update, 1.0 / 60.0)
        return game



if __name__ == '__main__':
    PongApp().run()
