### This is how you run:
1. Install the shit you need  
2. ``` python main.py ```  
3. open another powershell window and run: ``` python main.py ```

### Windows setup
+ Install [Microsoft Visual C++ Compiler for Python 2.7](http://aka.ms/vcpython27)
+ Install pip [following these instructions](https://github.com/BurntSushi/nfldb/wiki/Python-&-pip-Windows-installation#pip-install)
+ Ensure pip, wheel, and setuptools are installed  
``` pip install --upgrade pip wheel setuptools ```
+ Install dependencies with pip:  
``` pip install -r requirements.txt ```

### This can run on your phone
Install Kivy Launcher app for android
Put this dir on your phone and open it up in there!